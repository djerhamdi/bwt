
#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <algorithm>


void rotate(std::string &a) {
    char t = a[a.length() - 1];
    for (int i = a.length() - 1; i > 0; i--) {
        a[i] = a[i - 1];
    }
    a[0] = t;
}

int main(int argc, char const *argv[])
{

    std::string dataInput("abracadabra");
    std::vector<unsigned char> dataInputInChar (dataInput.begin(), dataInput.end());
    std::vector<unsigned char> bwtOutput;
    uint rank;
    std::vector<std::string> cyclicRotation;
    
    // cyclic rotation
  for (uint i = 0; i < dataInput.length(); i++)
    {
        
            cyclicRotation.push_back(dataInput);
            rotate(dataInput);
    }
   


    // print rotated tab
    for (int i = 0; i < cyclicRotation.size(); i++)
    {
            std::cout << cyclicRotation[i] <<std::endl ;
    }

    std::cout << std::endl;

    //  sort tab

    std::sort(cyclicRotation.begin(), cyclicRotation.end()); //rename cyclicTable to matrix // ajouter des fonctions avec des noms explicites 

    // print sorted rotated tab
    for (uint i = 0; i < cyclicRotation.size(); i++)
    {
        for (int j = 0; j < cyclicRotation[i].size(); j++)
            std::cout << cyclicRotation[i][j] ;
        std::cout << std::endl;
    }

    // get rank
    for (uint i = 0; i < cyclicRotation.size(); i++)
    {
        if (cyclicRotation[i] == dataInput)
        {
            rank = i;
            break;
        }
    }

    std::cout << "rang = " << rank << std::endl;

    // get bwtOutput
    bwtOutput.push_back(rank  &  255);
    bwtOutput.push_back(rank >> 8 & 255);
    bwtOutput.push_back(rank >> 16 & 255);
    bwtOutput.push_back(rank >> 24 & 255);

    uint lastColumnindex = cyclicRotation[0].size()-1;
    for (uint i =0; i <= lastColumnindex ;i++){ // matrix
        bwtOutput.push_back(cyclicRotation[i][lastColumnindex]); // i = line, 

    }
    std::string dataOutput(bwtOutput.begin(), bwtOutput.end());
    std::cout << dataOutput << std::endl;
    // print bwtoutput
   for (uint i = 0; i < bwtOutput.size(); i++)
    {
     std::cout << bwtOutput[i];
    }

    //std::cout <<std::endl << dataOutput;
    //printf ("%d",dataOutput);

    return 0;
}
