#include "bwt.h"

int main(int argc, char const *argv[])
{
    if (argc < 2)
    {

        printf("ERROR - add filename as first argument \n");
        return -1;
    }

    bwt obj;
    std::vector<unsigned char> datainput;
    std::vector<unsigned char> bwtOutput;
    int length;

    FILE *f = fopen(argv[1], "r");
    if (f == NULL)
    {
        std::cout << "ERROR - failed to open file: " << argv[1] << std::endl;
        return -1;
    }

    // reading file
    fseek(f, 0, SEEK_END);
    length = ftell(f);
    fseek(f, 0, SEEK_SET);
    char *buffer = (char *)malloc(length);
    fread(buffer, 1, length, f);
    fclose(f);
    for (uint i = 0; i < length; i++)
    {
        datainput.push_back(buffer[i]);
    }
    std::cout << "datainput size = " << datainput.size() << std::endl;
    std::cout <<"function is being called"<<std::endl;
    bwtOutput = obj.bwTransform(datainput);
    for (uint i = 0; i < bwtOutput.size(); i++)
    {
        std::cout << bwtOutput[i];
    }
    std::cout << std::endl;

    free(buffer);
    return 0;
}
