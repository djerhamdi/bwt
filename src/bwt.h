#pragma once
#include <iostream>
#include <string>
#include <bitset>
#include <vector>
#include <algorithm>
#include <chrono>

class bwt
{
public:
    std::vector<unsigned char> bwTransform(std::vector<unsigned char> dataInput);
    std::vector<unsigned char> str2CharVector(std::string stringInput);
    std::chrono::time_point<std::chrono::steady_clock> get_time();
    void rotate(std::vector<unsigned char> &a);
};