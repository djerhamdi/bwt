#include "bwt.h"
std::vector<unsigned char> bwt::bwTransform(std::vector<unsigned char> dataInput)
{
    double ms;
    std::chrono::duration<double> duration;
    std::chrono::time_point<std::chrono::steady_clock> tic, toc;
    std::vector<unsigned char> bwtOutput;
    std::vector<std::vector <unsigned char> > cyclicRotation (dataInput.size(),std::vector <unsigned char>(dataInput.size()));
    uint rank;

    // cyclic rotation
    tic = get_time();
    for (uint i = 0; i < dataInput.size(); i++)
    {

        cyclicRotation[i]=dataInput;
        rotate(dataInput);
    }
    toc = get_time();
    duration = toc - tic;
    ms = duration.count() * 1000.0f;
    std::cout << "Cyclic rotation took =\t" << ms << "\tms" << std::endl;

    //  sort tab

    tic = get_time();
    std::sort(cyclicRotation.begin(), cyclicRotation.end());
    toc = get_time();
    duration = toc - tic;
    ms = duration.count() * 1000.0f;
    std::cout << "Sorting 2D table took =\t" << ms << "\tms" << std::endl;

    // get rank
    tic = get_time();
    for (uint i = 0; i < cyclicRotation.size(); i++)
    {
        if (cyclicRotation[i] == dataInput)
        {
            rank = i;
            break;
        }
    }
    toc = get_time();
    duration = toc - tic;
    ms = duration.count() * 1000.0f;
    std::cout << "Finding rank took =\t" << ms << "\tms" << std::endl;

    // get bwtOutput
    tic = get_time();
    bwtOutput.push_back(rank & 255);
    bwtOutput.push_back(rank >> 8 & 255);
    bwtOutput.push_back(rank >> 16 & 255);
    bwtOutput.push_back(rank >> 24 & 255);
    uint lastColumnindex = cyclicRotation[0].size() - 1;
    for (uint i = 0; i <= lastColumnindex; i++)
    {
        bwtOutput.push_back(cyclicRotation[i][lastColumnindex]);
    }
    toc = get_time();
    duration = toc - tic;
    ms = duration.count() * 1000.0f;
    std::cout << "filling BWT output vector took =\t" << ms << "\tms" << std::endl;

    return bwtOutput;
}

std::vector<unsigned char> bwt::str2CharVector(std::string stringInput)
{

    std::vector<unsigned char> charVector(stringInput.begin(), stringInput.end());

    return charVector;
}

std::chrono::time_point<std::chrono::steady_clock> bwt::get_time()
{

    std::chrono::time_point<std::chrono::steady_clock> tick = std::chrono::steady_clock::now();
    return tick;
}
void bwt::rotate(std::vector <unsigned char> &a)
{
    char t = a[a.size() - 1];
    for (int i = a.size() - 1; i > 0; i--)
    {
        a[i] = a[i - 1];
    }
    a[0] = t;
}